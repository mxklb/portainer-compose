# Portainer Compose
A simple docker-compose file to spin up [portainer](www.portainer.io/)
for a local docker host.

To run it, just call ..

    docker-compose up -d

Note: Set admin password during first boot/access. 
