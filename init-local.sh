#!/bin/bash
# Clean up & Initialize fresh local folders ..
data_dir="/srv/docker"

# Set data folder from environment
[[ -z "${DATA_FOLDER}" ]] || data_dir="${DATA_FOLDER}"

sudo rm -rf $data_dir/portainer
sudo mkdir -p $data_dir/portainer/data
